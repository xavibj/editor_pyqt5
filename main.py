from PyQt5.QtWidgets import QTextEdit, QWidget, QDialog, QApplication, QMainWindow, QFileDialog
import mainwindow
import sys


class Mainwindow(QMainWindow, mainwindow.Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.actionQuit.triggered.connect(self.close)
        self.actionOpen.triggered.connect(self.open)
        self.setFocus(True)

    def open(self):
        print("open")
        d = QFileDialog.getOpenFileName(self, "open", "/Users/xavibj")

        if not d[0] == '':
            file = open(d[0], 'r')
            self.setStatusTip(d[0])
            with file:
                text = file.read()
                self.textEdit.setText(text)


def main(argv):
    app = QApplication(argv)
    ui = Mainwindow()
    ui.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main(sys.argv)